$(function () {
	var actuNum = 1;

	load((anchor().length == 0) ? "home.html" : anchor()+".html");

	// Si il y a un changement d'encre dans l'URL
	$(window).on('hashchange', function() {
		var page = anchor()+".html"
		console.log("Try to load " + page);
		load(page);
	});

	$("#menu #menu-tool a").click(function () {
		var $nav = $("nav");
		if ($nav.hasClass("open")) {
			$nav.removeClass("open");
		}else {
			$nav.addClass("open");
			// $nav.addClass("open", 800, "easeOutBounce");
		}
	});

	$(".search input").on('focus', function (){
		var $search = $(".search");
		if ($search.hasClass("open")) {
			$search.removeClass("open");
		}else {
			$search.addClass("open");
			// $nav.addClass("open", 800, "easeOutBounce");
		}
	});

	$(document).on('click', '#actualites div.footer #actu-prev', function () {
		if (actuNum > 1) {
			changeActu(actuNum-1);
		}
	});

	$(document).on('click', '#actualites div.footer #actu-next', function () {
		if (actuNum < $(".actu").length) {
			changeActu(actuNum+1);
		}
	});

	// Change l'actualité courante (paramètre: l'indice de l'actualité à afficher)
	function changeActu(num) {
		if (actuNum != num) {
			$(".actu[data-actu='"+actuNum+"']").fadeOut('fast', function () {
				$(".actu[data-actu='"+num+"']").fadeIn();
			});
		}
		actuNum = num;
	}

	// Garde le champ de recherche actif ou non [à améliorer]
	$("#menu li.search input").blur(
		function () {
			if ($(this).val().length > 0) {
				$("#menu li.search").removeClass("open");
			}
		},
		function () {
			if ($(this).val().length == 0) {
				$("#menu li.search").removeClass("open");
			}
		}
	);
	$("#menu li.search input").hover(
		function () {
			if ($(this).val().length > 0) {
				$("#menu li.search").removeClass("open");
			}
		},
		function () {
			if ($(this).val().length > 0) {
				$("#menu li.search").addClass("open");
			}
		}
	);

	// Retourne l'ancre de l'url courante
	function anchor() {
		return window.location.hash.substring(1);
	}

	// Charge une page de façon asynchrone dans la section "content" du site (paramètre: le nom du fichier)
	function load(page) {
		$("#content").load(page);
	}
});